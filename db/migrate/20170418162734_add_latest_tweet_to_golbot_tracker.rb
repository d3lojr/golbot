class AddLatestTweetToGolbotTracker < ActiveRecord::Migration
  def change
    add_column :golbot_trackers, :latest_tweet, :string
  end
end
