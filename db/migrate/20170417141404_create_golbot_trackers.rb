class CreateGolbotTrackers < ActiveRecord::Migration
  def change
    create_table :golbot_trackers do |t|
      t.string :league
      t.string :key
      t.string :channel
      t.string :last_event

      t.timestamps null: false
    end
  end
end
