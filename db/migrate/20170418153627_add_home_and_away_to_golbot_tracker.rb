class AddHomeAndAwayToGolbotTracker < ActiveRecord::Migration
  def change
    add_column :golbot_trackers, :home, :string
    add_column :golbot_trackers, :away, :string
  end
end
