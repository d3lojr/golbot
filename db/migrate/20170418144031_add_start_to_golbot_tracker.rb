class AddStartToGolbotTracker < ActiveRecord::Migration
  def change
    add_column :golbot_trackers, :start, :datetime
  end
end
