class AddDateToGolbotMatch < ActiveRecord::Migration
  def up
    ::Golbot::Match.destroy_all
    add_column :golbot_matches, :date, :datetime
  end

  def down
    remove_column :golbot_matches, :date
  end
end
