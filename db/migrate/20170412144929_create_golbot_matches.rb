class CreateGolbotMatches < ActiveRecord::Migration
  def change
    create_table :golbot_matches do |t|
      t.string :league
      t.string :key
      t.string :home
      t.string :away
      t.string :score

      t.timestamps null: false
    end
  end
end
