class Table < SlackRubyBot::Commands::Base
  def self.build_teams_response(league, divisions)
    unless divisions.blank?
      response = ""
      divisions.each do |division|
        if division_name = division.attribute('name').try(:value)
          response += "\r\n" + division_name + " Division\r\n"
        end

        if ["mls","ucl","epl"].include?(league.downcase)
          teams = division.css('/team_standing')
          teams.each do |team|
            response += "[" + team.attribute('rank').value.rjust(2, '0') + "] "
            response += team.css('team').attribute('name').value.ljust(40, ' ')
            response += " (" + team.attribute('points').value + ")\r\n"
          end
        else
          teams = division.css('/team')
          teams.each do |team|
            response += team.attribute('name').value
            
            wins = team.attribute('wins')
            losses = team.attribute('losses')
            if wins.present? && losses.present?
              response += " (" + wins.value + " - " + losses.value + ")"
            end

            response += "\r\n"
          end
        end
      end
    else
      teams = doc.css('//team')
      teams.each do |team|
        response += team.attribute('name').value + "\r\n"
      end
    end
    return response
  end

  def self.call(client, data, match)
    current_season = 2016

    @sports_data_keys = [
      ['mlb', 'http://api.sportradar.us/mlb-t6/seasontd/' + current_season.to_s + '/REG/standings.xml?api_key=asqfm6mqjdpba8g66spvdupk'],
      ['nba', 'http://api.sportradar.us/nba-t3/seasontd/' + current_season.to_s + '/REG/standings.xml?api_key=jv6x6p5zkhtsetcxtptas3tn'],
      ['nfl', 'http://api.sportradar.us/nfl-ot1/seasontd/' + current_season.to_s + '/standings.xml?api_key=b8c9qs4npkd6v9xdpnfu2s7v'],
      ['nhl', 'http://api.sportradar.us/nhl-ot4/seasontd/' + current_season.to_s + '/REG/standings.xml?api_key=mrk2vxy9hzx58uxvnh8e22wz'],
      ['mls', 'http://api.sportradar.us/soccer-t3/am/en/tournaments/sr:tournament:242/standings.xml?api_key=7bm3wr5j5ry2spknw29899pw'],
      ['ucl', 'http://api.sportradar.us/soccer-t3/eu/en/tournaments/sr:tournament:7/standings.xml?api_key=n8uyrf5xykhbzjz45kszru48'],
      ['epl', 'http://api.sportradar.us/soccer-t3/eu/en/tournaments/sr:tournament:17/standings.xml?api_key=n8uyrf5xykhbzjz45kszru48']
    ]

    if match[:expression].present?
      # if match[:expression].to_i =~ /[0-9]{3}/
        # league_check = match[:expression].to_s

        # url = URI.parse('http://api.football-data.org/v1/competitions/' + league_check + '/leagueTable')
        # request = Net::HTTP::Get.new(url.to_s)
        # result = Net::HTTP.start(url.host, url.port) {|http|
        #   http.request(request)
        # }

        # result = JSON.parse(result.body)

        # unless result.blank?
        #   if result['error'].present?
        #     response = result['error']
        #   else
        #     teams = result['standing']

        #     response = "Here's the top four teams in #{result['leagueCaption']}:\n\r"
        #     teams.each_with_index do |team, i|
        #       if i < 4
        #         response += "[*" + team['position'].to_s + "*] " + team['teamName'] + " (" + team['points'].to_s + ")\n\r"
        #       end
        #     end
        #   end
        # else
        #   response = "Something went wrong"
        # end
      # elsif match[:expression].to_s.downcase =~ /[a-z]{3,6}/
      
        league_check = match[:expression].downcase
        match_found = false
        response = ""
        league_title = match[:expression].upcase

        @sports_data_keys.each do |league|
          next if league_check.present? && league[0] != league_check

          match_found = true

          url = URI.parse(league[1])
          result = Net::HTTP.get_response(url)

          if result.code == "200"
            doc = Nokogiri::XML.parse(result.body)
            
            if ["epl"].include?(match[:expression].downcase)
              league_obj = doc.css('tournament')
            else
              league_obj = doc.css('//league')
            end

            if league_obj.present?
              league_title = league_obj.attribute('name').try(:value)
            end

            response = "Here's the standings for #{league_title}:\r\n"

            if ["mls","ucl", "epl"].include?(match[:expression].downcase)
              divisions = doc.css('//standings').first.css('/group')
              response += build_teams_response(match[:expression].downcase, divisions)
            elsif ["nba","nhl"].include?(match[:expression].downcase)
              conferences = doc.css('//conference')
              conferences.each do |conference|
                unless conference.attribute('name').nil?
                  response += "\r\n" + conference.attribute('name').value + "\r\n"
                  divisions = conference.css('/division')
                  response += build_teams_response(match[:expression].downcase, divisions)
                end
              end
            else
              divisions = doc.css('//division')
              if divisions.blank?
                divisions = doc.css('//group')
              end
              unless divisions.blank?
                response += build_teams_response(match[:expression].downcase, divisions)
              end
            end

            response = '```' + response + '```' unless response.blank?
          else
            response = "[*"
            response += result.code
            response += "*] There was an error retrieving information from the API."
          end
        end

        if !match_found
          client.say(channel: data.channel, text: "I'm sorry, but I don't have a league table for #{league_title}.")
        elsif !response.blank?
          client.say(channel: data.channel, text: response)
        else
          client.say(channel: data.channel, text: "I'm sorry, but I don't recognize your request.")
        end
      # end
    else
      client.say(channel: data.channel, text: "Please specify a league.")
    end
  end
end
