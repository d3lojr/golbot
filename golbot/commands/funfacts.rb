class Funfacts < SlackRubyBot::Commands::Base
  match /funfacts\s(\w*)\s(and|vs)\s(\w*)/i do |client, data, match|
    if match[1].present? && match[3].present?
      team1 = match[1].downcase
      team2 = match[3].downcase

      replacements = [
        ['utd', 'united']
      ]

      replacements.each do |c|
        team1.gsub!(c[0], c[1])
        team2.gsub!(c[0], c[1])
      end

      team1.gsub!('the', '')
      team2.gsub!('the', '')

      team1.gsub!(/\s+/, '%')
      team2.gsub!(/\s+/, '%')

      matches = ::Golbot::Match.where("((home ILIKE ?) AND (away ILIKE ?)) OR ((home ILIKE ?) AND (away ILIKE ?))", '%'+team1+'%', '%'+team2+'%', '%'+team2+'%', '%'+team1+'%')

      unless matches.blank?
        if matches.count == 1
          match_id = matches.first.key
          league_key = matches.first.league

          @sports_data_keys = [
            ['mls', 'https://api.sportradar.us/soccer-t3/am/en/matches/sr:match:', '/funfacts.xml?api_key=7bm3wr5j5ry2spknw29899pw'],
            ['ucl', 'https://api.sportradar.us/soccer-t3/eu/en/matches/sr:match:', '/funfacts.xml?api_key=n8uyrf5xykhbzjz45kszru48'],
            ['epl', 'https://api.sportradar.us/soccer-t3/eu/en/matches/sr:match:', '/funfacts.xml?api_key=n8uyrf5xykhbzjz45kszru48']
          ]

          match_found = false
          response = ""

          @sports_data_keys.each do |league|
            next if league_key.present? && league[0] != league_key

            match_found = true

            url = URI.parse(league[1] + match_id + league[2])
            result = Net::HTTP.get_response(url)

            if result.code == "200"
              doc = Nokogiri::XML.parse(result.body)

              response = (matches.first.home + ' vs ' + matches.first.away + ' (' + matches.first.score + ')')
              response += "\r\n"

              facts = doc.css('fact')

              unless facts.blank?
                facts.each_with_index do |fact, i|
                  if (statement = fact.attribute('statement').try(:value))
                    response += "#{i+1}: _#{statement}_\r\n"
                  end
                end
              end
            else
              response = "[*"
              response += result.code
              response += "*] There was an error retrieving information from the API."
            end
          end

          if match_found && !response.blank?
            client.say(channel: data.channel, text: response)
          else
            client.say(channel: data.channel, text: (matches.first.home + ' vs ' + matches.first.away + ' (' + matches.first.score + ')'))
          end
        else
          client.say(channel: data.channel, text: 'Multiple matches found!')
        end
      else
        client.say(channel: data.channel, text: 'No matches found. Searched for: _' + team1.titleize + ' & ' + team2.titleize + '_.')
      end
    else
      client.say(channel: data.channel, gif: 'shrug')
    end
  end
end