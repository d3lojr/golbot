class Leagues < SlackRubyBot::Commands::Base
  command 'leagues' do |client, data, _match|
    leagues = Bot.leagues
    
    unless leagues.blank?
      response = "I'm currently tracking the following leagues:\r\n"
      leagues.each do |key, value|
        response += "[*#{key}*] #{value}\r\n"
      end
    else
      response = "I'm not currently setup to track any leagues"
    end

    client.say(channel: data.channel, text: response)
  end
end
