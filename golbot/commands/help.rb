class Help < SlackRubyBot::Commands::Base
  command 'help' do |client, data, _match|
    response = "Commands:\r\n"
    response += "[*leagues*]: Lists all leagues.\r\n"
    # response += "[*competitions*]: Lists all soccer competitions.\r\n"
    # response += "_You must specify a league (#{Bot.leagues.keys[0..2].join(', ')}, etc.) for the following commands:_\r\n"
    # response += "[*table <league>*]: Retrieve current League Table for a given league/competition.\r\n"
    response += "[*standings <league>*]: Retrieve current standings for a given league/competition.\r\n"
    response += "\r\n"
    response += "[*today*]: Retrieve today's matches.\r\n"
    response += "[*yesterday*]: Retrieve yesterday's matches.\r\n"
    response += "[*tomorrow*]: Retrieve tomorrow's matches.\r\n"
    response += "\r\n"
    response += "[*this weekend*]: Retrieve upcoming weekend's matches.\r\n"
    response += "[*last weekend*]: Retrieve last weekend's matches.\r\n"
    response += "\r\n"
    response += "[*find <team>*]: Get recent and upcoming matches for *<team1>*.\r\n"
    response += "[*track <team1>* vs *<team2>*]: Get live updates on a single, live match.\r\n"
    client.say(channel: data.channel, text: response)
  end
end
