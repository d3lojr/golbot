class Tomorrow < SlackRubyBot::Commands::Base
  def self.call(client, data, match)
    total_matches = 0
    response = ""
    Bot.leagues.keys.each do |league|
      matches = Bot.find_matches_by_league_and_date(league.downcase, Time.now.in_time_zone.to_date.tomorrow)

      unless matches.blank?
        total_matches = total_matches + matches.count
        response += "Matches for #{Bot.leagues[league]}:\r\n"
        matches.each do |game|
          if game.date.present?
            response += "[*#{game.date.in_time_zone.strftime('%a, %b %-d, %-l:%M:%S %P')}*] "
          else
            response += "[*Time Unavailable*] "
          end
          response += "#{game.home} vs #{game.away}"
          if game.score.present?
            response += " (#{game.score})"
          end
          response += "\r\n"
        end
      end
    end

    if total_matches < 1
      client.say(channel: data.channel, text: 'No matches found. Searched for: _' + Time.now.in_time_zone.to_date.tomorrow.strftime('%a, %b %-d') + '_.')
    elsif !response.blank?
      client.say(channel: data.channel, text: response)
    end
  end
end
