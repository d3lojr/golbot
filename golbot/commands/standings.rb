class Standings < SlackRubyBot::Commands::Base
  def self.call(client, data, match)
    require 'net/http'

    leagues = Bot.leagues
    rows = []
    maxlens = []

    # TODO: Add a check if it's postseason/playoffs if applicable

    case match[:expression].try(:downcase)

    when "nfl"
      url = URI.parse("http://www.nfl.com/standings")
      result = Net::HTTP.get_response(url)

      if result.code == "200"
        doc = Nokogiri::HTML.parse(result.body)

        standings = doc.css('table')
        conferences = standings.css('tbody')
        conferences.each do |conference|
          # conference_title = conference.css('tr:first-child').try(:text).try(:strip)

          table_rows = conference.css('tr')
          # shift to get rid of conference title
          table_rows.shift
          table_rows.each_with_index do |tr, i|
            if (i%6 == 0)
              # This is a division title
              division_title = tr.css('td:first-child').try(:text).try(:strip)
              rows << ['#', division_title, 'W', 'L', 'T', 'Pct', 'PF', 'PA', 'TD', 'L5']
            elsif ((i+1)%6 == 0)
              next
            else
              team_name = tr.css('td:first-child').try(:text).try(:strip)

              rows << [
                (i%6).to_s, team_name,
                tr.css('td')[1].try(:text).try(:strip),
                tr.css('td')[2].try(:text).try(:strip),
                tr.css('td')[3].try(:text).try(:strip),
                tr.css('td')[4].try(:text).try(:strip),
                tr.css('td')[5].try(:text).try(:strip),
                tr.css('td')[6].try(:text).try(:strip),
                tr.css('td')[8].try(:text).try(:strip),
                tr.css('td').last.try(:text).try(:strip)
              ]
            end
          end
        end
      else
        client.say(channel: data.channel, gif: ['derp', 'error', 'confused', '404'].sample)
      end
    when "nhl"
      url = URI.parse("https://statsapi.web.nhl.com/api/v1/standings")
      result = Net::HTTP.get_response(url)

      if result.code == "200"
        doc = JSON.parse(result.body)

        records = doc['records']

        records.each do |record|
          rows << ['#',record['division']['name'],'GP','W','L','OT','PTS','GF','GA']

          record['teamRecords'].each do |team|
            rows << [
              team['divisionRank'].to_s, team['team']['name'].to_s, team['gamesPlayed'].to_s,
              team['leagueRecord']['wins'].to_s, team['leagueRecord']['losses'].to_s, team['leagueRecord']['ot'].to_s,
              team['points'].to_s, team['goalsScored'].to_s, team['goalsAgainst'].to_s
            ]
          end
        end
      else
        client.say(channel: data.channel, gif: ['derp', 'error', 'confused', '404'].sample)
      end
    when "epl"
      url = URI.parse("http://soccer.sportsopendata.net/v1/leagues/premier-league/seasons/17-18/standings")
      result = Net::HTTP.get_response(url)

      if result.code == "200"
        doc = JSON.parse(result.body)

        standings = doc['data']['standings']

        rows << ['#','Premier League','GP','W','D','L','GF','GA','GD','Pts']

        standings.each do |team|
          columns = [
            team['position'].to_s, team['team'], team['overall']['matches_played'].to_s,
            team['overall']['wins'].to_s, team['overall']['draws'].to_s, team['overall']['losts'].to_s,
            team['overall']['scores'].to_s, team['overall']['conceded'].to_s,
            (team['overall']['scores'].to_i - team['overall']['conceded'].to_i).to_s,
            team['overall']['points'].to_s
          ]

          # ['overall','home','away'].each do |g|
          #   columns << team[g]['wins'].to_s
          #   columns << team[g]['draws'].to_s
          #   columns << team[g]['losts'].to_s
          # end

          rows << columns
        end
      else
        client.say(channel: data.channel, gif: ['derp', 'error', 'confused', '404'].sample)
      end
    when "mlb"
      fields = ["team_full","w","l","pct","gb","home","away","last_ten","streak"]
      fields_titles = ["W","L","W %","GB","Home","Road","L 10", "Streak"]

      url = URI.parse("http://mlb.com/lookup/json/named.standings_schedule_date.bam?season=2017&schedule_game_date.game_date=%27" +
        Date.today.strftime('%Y/%m/%d') + "%27&sit_code=%27h0%27&league_id=103&league_id=104&version=2")
      result = Net::HTTP.get_response(url)
      if result.code == "200"
        standings = JSON.parse(result.body)
        conferences = standings['standings_schedule_date']['standings_all_date_rptr']['standings_all_date']

        conferences.each do |conference|
          conference_title = ["•",conference['queryResults']['row'].first['division'].split(' ')[0..-2].join(' ')]
          (fields.size - 1).times do |i|
            conference_title << " "
          end
          rows << conference_title

          teams = conference['queryResults']['row']
          i = 0
          teams.each do |team|
            if (i == 0 || (teams[i-1]['division'] != teams[i]['division']))
              division_title = ["#",teams[i]['division']]
              (fields.size - 1).times do |j|
                division_title << fields_titles[j]
              end
              rows << division_title

              i = 0
            end

            columns = [(i+1).to_s]

            fields.each_with_index do |td, j|
              if td == "team_full"
                columns << " " + team[td]
              else
                columns << team[td]
              end
            end

            rows << columns
            i += 1
          end
        end
      else

        client.say(channel: data.channel, gif: ['derp', 'error', 'confused', '404'].sample)
      end
    when "nba"
      # NBA has a JSON feed. Fields we would like to display are below.
      fields = ["teamId","win","loss","winPctV2","gamesBehind","home","road","l10","streak"]
      # Column Titles: This should be one less than the array above.
      fields_titles = ["W","L","Win %","GB","Home","Road","L 10", "Streak"]

      # Get teams first so we can replace teamId
      url = URI.parse("http://data.nba.net/data/10s/prod/v1/2016/teams.json")
      result = Net::HTTP.get_response(url)

      if result.code == "200"
        teams = JSON.parse(result.body)
        teams = teams['league']['standard']

        url = URI.parse("http://data.nba.net/data/10s/prod/v1/current/standings_conference.json")
        result = Net::HTTP.get_response(url)

        if result.code == "200"
          standings = JSON.parse(result.body)

          # This is formatted kinda strange:
          #  {"conference" => ["east", team1, team2, ..]}, {"conference" => ["west", team1, team2, ..]}
          standings['league']['standard']['conference'].each do |table|
            # Build column titles.
            conference_title = ['#',"• #{table.first.titleize} Conference"]
            (fields.size - 1).times do |i|
              conference_title << fields_titles[i]
            end
            rows << conference_title

            table.second.each_with_index do |row, i|
              columns = [i.to_s]

              fields.each_with_index do |td, i|
                if td == "teamId"
                  value = " " + teams.find{|t| t['teamId'] == row[td]}['fullName']
                elsif td == "gamesBehind"
                  value = row[td].gsub(/^0$/,'-')
                elsif td == "home"
                  value = "#{row['homeWin']} - #{row['homeLoss']}"
                elsif td == "road"
                  value = "#{row['awayWin']} - #{row['awayLoss']}"
                elsif td == "l10"
                  value = "#{row['lastTenWin']} - #{row['lastTenLoss']}"
                elsif td == "streak"
                  value = (row['isWinStreak'] == true ? '+ ' : '- ') + "#{row['streak']}"
                else
                  value = row[td]
                end

                columns << value
              end

              rows << columns
            end
          end
        else
          client.say(channel: data.channel, gif: ['derp', 'error', 'confused', '404'].sample)
        end
      else
        client.say(channel: data.channel, gif: ['derp', 'error', 'confused', '404'].sample)
      end
    when "mls"
      url = URI.parse("https://www.mlssoccer.com/standings")
      result = Net::HTTP.get_response(url)

      if result.code == "200"
        doc = Nokogiri::HTML.parse(result.body)

        standings = doc.css('.standings_table')
        standings.each do |table|
          table.css('tbody tr').each do |row|
            columns = []

            row.css('td').each_with_index do |td, i|
              unless td.text.strip.blank?
                if td.text.strip == "W-L-T" && i == 13
                  columns << "Home"
                elsif td.text.strip == "W-L-T" && i == 15
                  columns << "Away"
                else
                  columns << td.text.strip
                end
              end
            end

            rows << columns unless columns.blank?
          end
        end
      else
        client.say(channel: data.channel, gif: ['derp', 'error', 'confused', '404'].sample)
      end
    else
      client.say(channel: data.channel, text: "Sorry, that's not a league I recognize.\r\nTry: " + leagues.collect{|c| '*'+c+'*'}.join(", "))
    end

    unless rows.blank?
      rows.each do |row|
        row.each_with_index do |column, i|
          if(maxlens[i].nil? || column.length > maxlens[i])
            maxlens[i] = column.length
          end
        end
      end

      client.say(channel: data.channel, text: "Here's the standings for the #{match[:expression].try(:upcase)}:\r\n" + '```' + rows.collect{ |r|
        r.each_with_index.collect{ |c,i|
          if maxlens[i] < 3
            c.rjust(3)
          elsif maxlens[i] < 5
            c.rjust(5)
          else
            c.ljust(maxlens[i])
          end
        }.join("  ")
      }.join("\r\n") + '```')
    end
  end
end