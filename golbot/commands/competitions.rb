class Competitions < SlackRubyBot::Commands::Base
  command 'competitions' do |client, data, _match|
    url = URI.parse('http://api.football-data.org/v1/competitions/?season=2016')
    request = Net::HTTP::Get.new(url.to_s)
    result = Net::HTTP.start(url.host, url.port) {|http|
      http.request(request)
    }

    teams = JSON.parse(result.body)

    unless teams.blank?
      response = "I am currently tracking the following competitions:\n\r"
      teams.each do |team|
        response += "[*" + team['id'].to_s + "*] " + team['caption'] + "\n\r"
      end
    else
      response = "There are currently no competitions being tracked."
    end

    client.say(channel: data.channel, text: response)
  end
end
