class Track < SlackRubyBot::Commands::Base
  match /track-id\s(\d+)\Z/i do |client, data, match|
    game = ::Golbot::Match.find_by(id: match[1].to_i)
    unless game.nil?
      if tracker = ::Golbot::Tracker.find_or_create_by(
        league: game.league,
        key: game.key,
        home: game.home,
        away: game.away,
        start: game.date,
        channel: data.channel
      )
        # client.say(channel: data.channel, text: "I set up a tracker for you.\r\n> #{tracker.home} vs #{tracker.away} at #{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}")
        case game.league
        # when 'nhl'
          # http://statsapi.web.nhl.com/api/v1/game/#{tracker.key}/feed/live
        when 'nba'
          client.web_client.chat_postMessage(channel: data.channel, as_user: true,
            attachments: [
              {
                fallback: "New tracker created for you. #{tracker.home} vs #{tracker.away} at #{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                title: "#{tracker.home} vs #{tracker.away}",
                title_link: "http://www.cbssports.com/nba/gametracker/preview/NBA_#{tracker.key}/",
                text: "#{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                color: "#7CD197"
              }
            ].to_json
          )
        when 'epl', 'ucl', 'mls'
          client.web_client.chat_postMessage(channel: data.channel, as_user: true,
            attachments: [
              {
                fallback: "New tracker created for you. #{tracker.home} vs #{tracker.away} at #{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                title: "#{tracker.home} vs #{tracker.away}",
                title_link: "http://www.espnfc.us/match?gameId=#{tracker.key}",
                text: "#{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                color: "#7CD197"
              }
            ].to_json
          )
        else
          client.web_client.chat_postMessage(channel: data.channel, as_user: true,
            attachments: [
              {
                fallback: "New tracker created for you. #{tracker.home} vs #{tracker.away} at #{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                title: "#{tracker.home} vs #{tracker.away}",
                text: "#{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                color: "#7CD197"
              }
            ].to_json
          )
        end
      else
        client.say(channel: data.channel, text: "Failed to create tracker for #{match[0]}")
      end
    else
      client.say(channel: data.channel, text: "Failed to locate match [*#{match[1].to_i}*]")
    end
  end

  match /track\s(.*)\s(and|vs)\s(.*)\Z/i do |client, data, match|
    if match[1].present? && match[3].present?
      team1 = match[1].downcase
      team2 = match[3].downcase

      matches = Bot.find_upcoming_matches_by_teams(team1, team2)

      unless matches.blank?
        if matches.size == 1
          game = matches.first
          if tracker = ::Golbot::Tracker.find_or_create_by(
            league: game.league,
            key: game.key,
            home: game.home,
            away: game.away,
            start: game.date,
            channel: data.channel
          )
            case game.league
            # when 'nhl'
              # http://statsapi.web.nhl.com/api/v1/game/#{tracker.key}/feed/live
            when 'nba'
              client.web_client.chat_postMessage(channel: data.channel, as_user: true,
                attachments: [
                  {
                    fallback: "New tracker created for you. #{tracker.home} vs #{tracker.away} at #{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                    title: "#{tracker.home} vs #{tracker.away}",
                    title_link: "http://www.cbssports.com/nba/gametracker/live/NBA_#{tracker.key}/",
                    text: "#{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                    color: "#7CD197"
                  }
                ].to_json
              )
            when 'epl', 'ucl', 'mls'
              client.web_client.chat_postMessage(channel: data.channel, as_user: true,
                attachments: [
                  {
                    fallback: "New tracker created for you. #{tracker.home} vs #{tracker.away} at #{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                    title: "#{tracker.home} vs #{tracker.away}",
                    title_link: "http://www.espnfc.us/match?gameId=#{tracker.key}",
                    text: "#{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                    color: "#7CD197"
                  }
                ].to_json
              )
            else
              client.web_client.chat_postMessage(channel: data.channel, as_user: true,
                attachments: [
                  {
                    fallback: "New tracker created for you. #{tracker.home} vs #{tracker.away} at #{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                    title: "#{tracker.home} vs #{tracker.away}",
                    text: "#{tracker.start.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')}",
                    color: "#7CD197"
                  }
                ].to_json
              )
            end
          else
            client.say(channel: data.channel, text: "Failed to create tracker for #{match_id}")
          end
        else
          client.say(channel: data.channel, text: "Multiple matches found. Try using `track-id`.\r\nSearched for: _#{team1.titleize} & #{team2.titleize}_.")
          matches.each do |game|
            response = "[*#{game.id.to_s.rjust(6,'0')}*] #{game.home} vs #{game.away}"
            response += " _(#{game.date.in_time_zone.strftime('%a, %b %-d %-l:%M:%S %P')})_"
            client.say(channel: data.channel, text: response)
          end
        end
      else
        client.say(channel: data.channel, text: 'No matches found. Searched for: _' + team1.titleize + ' & ' + team2.titleize + '_.')
      end
    else
      client.say(channel: data.channel, gif: 'shrug')
    end
  end

  match /trackers\s?(.*)?\Z/ do |client, data, match|
    trackers = []

    ::Golbot::Tracker.all.each do |tracker|
      response = "[*#{tracker.id}*] #{tracker.home} vs #{tracker.away} _(#{tracker.channel})_"

      # web_client = Slack::Web::Client.new(token: Team.first.token)
      # username = web_client.users_info(user: tracker.channel).user.real_name
      # response += "_(#{username})_"

      trackers << response
    end

    unless trackers.blank?
      client.say(channel: data.channel, text: trackers.join("\r\n"))
    else
      client.say(channel: data.channel, gif: 'nothing')
    end
  end

  match /kill-all\Z/ do |client, data, match|
    trackers = ::Golbot::Tracker.all.count

    unless trackers == 0
      ::Golbot::Tracker.destroy_all

      client.say(channel: data.channel, text: 'Killed', gif: ['boom','explosion','poof','fire'].sample)
    else
      client.say(channel: data.channel, gif: 'nothing')
    end
  end
end
