class Weekend < SlackRubyBot::Commands::Base
  match /this weekend\Z/ do |client, data, match|
    date_check_saturday = Time.now.in_time_zone.to_date - Time.now.in_time_zone.to_date.wday + 6.days
    date_check_sunday = date_check_saturday + 1.day

    total_matches = 0
    response = ""
    [date_check_saturday, date_check_sunday].each do |date|
      response += "Matches for _#{date.strftime('%a, %b %-d')}_.\r\n"
      Bot.leagues.keys.each do |league|
        matches = Bot.find_matches_by_league_and_date(league.downcase, date)

        unless matches.blank?
          total_matches = total_matches + matches.count
          response += "Matches for #{Bot.leagues[league]}:\r\n"
          matches.each do |game|
            if game.date.present?
              response += "[*#{game.date.in_time_zone.strftime('%a, %b %-d, %-l:%M:%S %P')}*] "
            else
              response += "[*Time Unavailable*] "
            end
            response += "#{game.home} vs #{game.away}"
            if game.score.present?
              response += " (#{game.score})"
            end
            response += "\r\n"
          end
        end
      end
    end

    if total_matches < 1
      client.say(channel: data.channel, text: 'No matches found. Searched for: _' + date_check_saturday.strftime('%a, %b %-d') + '_.')
    elsif !response.blank?
      client.say(channel: data.channel, text: "There is a total of *#{total_matches}* matches being played this weekend:\r\n" + response)
    end
  end

  match /last weekend\Z/ do |client, data, match|
    date_check_saturday = Time.now.in_time_zone.to_date - Time.now.in_time_zone.to_date.wday - 1.day
    date_check_sunday = date_check_saturday + 1.day

    total_matches = 0
    response = ""
    [date_check_saturday, date_check_sunday].each do |date|
      response += "Matches for _#{date.strftime('%a, %b %-d')}_.\r\n"
      Bot.leagues.keys.each do |league|
        matches = Bot.find_matches_by_league_and_date(league.downcase, date)

        unless matches.blank?
          total_matches = total_matches + matches.count
          response += "Matches for #{Bot.leagues[league]}:\r\n"
          matches.each do |game|
            if game.date.present?
              response += "[*#{game.date.in_time_zone.strftime('%a, %b %-d, %-l:%M:%S %P')}*] "
            else
              response += "[*Time Unavailable*] "
            end
            response += "#{game.home} vs #{game.away}"
            if game.score.present?
              response += " (#{game.score})"
            end
            response += "\r\n"
          end
        end
      end
    end

    if total_matches < 1
      client.say(channel: data.channel, text: 'No matches found. Searched for: _' + date_check_saturday.strftime('%a, %b %-d') + '_.')
    elsif !response.blank?
      client.say(channel: data.channel, text: "There was a total of *#{total_matches}* matches played last weekend:\r\n" + response)
    end
  end
end
