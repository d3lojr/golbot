class Search < SlackRubyBot::Commands::Base
  # match /time/i do |client, data, match|
  #   client.say(channel: data.channel, text: Time.now.in_time_zone.to_s)
  # end

  match /how\smany\smatches/i do |client, data, match|
    response = ""
    total_matches = 0

    Bot.leagues.each do |league, league_title|
      response += "_#{league_title}_: "
      num_of_matches = ::Golbot::Match.where(league: league.downcase).count
      total_matches += num_of_matches
      if num_of_matches > 0
        response += "#{num_of_matches} matches.\r\n"
      else
        response += "No matches.\r\n"
      end
    end

    if total_matches > 0
      client.say(channel: data.channel, text: "I currently have information on a total of *#{total_matches} match#{(total_matches > 1 ? 'es' : '')}*.\r\n" + response)
    else
      client.say(channel: data.channel, text: 'No matches found.', gif: ['nothing', 'empty', '404', 'wut'].sample)
    end
  end

  match /\Afind\s(.*)\s(and|vs)\s(.*)\z/i do |client, data, match|
    if match[1].present? && match[3].present?
      team1 = match[1].downcase
      team2 = match[3].downcase

      total_matches = 0
      response = ""
      Bot.leagues.keys.each do |league|
        matches = Bot.find_matches_by_league_and_teams(league.downcase, team1, team2)

        unless matches.blank?
          total_matches = total_matches + matches.count
          response += "Matches for #{Bot.leagues[league]}:\r\n"
          matches.each do |game|
            if game.date.present?
              response += "[*#{game.date.in_time_zone.strftime('%a, %b %-d, %-l:%M:%S %P')}*] "
            else
              response += "[*Time Unavailable*] "
            end
            response += "#{game.home} vs #{game.away}"
            if game.score.present?
              response += " (#{game.score})"
            end
            response += "\r\n"
          end
        end
      end

      if total_matches < 1
        client.say(channel: data.channel, text: 'No matches found. Searched for: _' + team1.titleize + ' & ' + team2.titleize + '_.')
      elsif !response.blank?
        client.say(channel: data.channel, text: response)
      end
    else
      client.say(channel: data.channel, gif: 'shrug')
    end
  end

  match /\Afind\s(.*)\z/i do |client, data, match|
    if match[1].present?
      team = match[1].downcase

      total_matches = 0
      response = ""
      Bot.leagues.keys.each do |league|
        matches = Bot.find_matches_by_league_and_team(league.downcase, team)

        unless matches.blank?
          total_matches = total_matches + matches.count
          response += "Matches for #{Bot.leagues[league]}:\r\n"
          matches.each do |game|
            if game.date.present?
              response += "[*#{game.date.in_time_zone.strftime('%a, %b %-d, %-l:%M:%S %P')}*] "
            else
              response += "[*Time Unavailable*] "
            end
            response += "#{game.home} vs #{game.away}"
            if game.score.present?
              response += " (#{game.score})"
            end
            response += "\r\n"
          end
        end
      end

      if total_matches < 1
        client.say(channel: data.channel, text: 'No matches found. Searched for: _' + team.titleize + '_.')
      elsif !response.blank?
        client.say(channel: data.channel, text: response)
      end
    else
      client.say(channel: data.channel, gif: 'shrug')
    end
  end

  match /who\swon(\sthe)?\s(.*)\s(and|vs)\s(.*)\Z/i do |client, data, match|
    if match[2].present? && match[4].present?
      team1 = match[2].downcase
      team2 = match[4].downcase

      matches = Bot.find_completed_matches_by_teams(team1, team2)

      unless matches.blank?
        matches.each do |game|
          if game.date.present?
            response += "[*#{game.date.in_time_zone.strftime('%a, %b %-d, %-l:%M:%S %P')}*] "
          else
            response += "[*Time Unavailable*] "
          end
          response += "#{game.home} vs #{game.away}"
          if game.score.present?
            response += " (#{game.score})"
          end
          client.say(channel: data.channel, text: response)
        end
      else
        client.say(channel: data.channel, text: "I couldn't find any completed matches. Searched for: _#{team1.titleize} & #{team2.titleize}_.")
      end
    else
      client.say(channel: data.channel, gif: 'shrug')
    end
  end
end
