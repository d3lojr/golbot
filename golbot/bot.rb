class Bot < SlackRubyBot::Commands::Base
  delegate :client, to: :instance

  def self.leagues
    {
      "NFL" => "National Football League",
      "MLB" => "Major League Baseball",
      "MLS" => "Major League Soccer",
      "NBA" => "National Basketball Association",
      "NHL" => "National Hockey League",
      "EPL" => "English Premier League",
      "UCL" => "UEFA Champion's League"
    }
  end

  def self.run
    instance.run
  end

  def self.instance
    SlackRubyBot::App.instance
  end

  def self.get_nfl_matches(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    # NFL is stupid and does this by "weeks"
    # TODO: Find a way to retrieve by a specific date
    matches_created = []
    url = "http://www.nfl.com/schedules"
    uri = URI.parse(url)

    response = Net::HTTP.get_response(uri)

    if response.code == "200"
      doc = Nokogiri::HTML.parse(response.body)

      games = doc.css('ul.schedules-table').last
      games = games.css('li')

      date_string = ""

      games.each do |game|
        # Check to see if this LI has the "schedules-list-date" class
        if (game.values.first.split(" ").include?("schedules-list-date"))
          # This is a row with a date. Need to adjust date.
          date_string = game.css('span:first-child span:first-child').try(:text)
        else
          # This is a row with a game.

          # Make sure we have a valid date
          unless date_string.blank?
            home_team = game.css('span.team-name.home').try(:text).try(:strip)
            away_team = game.css('span.team-name.away').try(:text).try(:strip)

            match_time_string = game.css('span.time').try(:text).try(:strip)
            match_time_string += game.css('span.suff span:first-child').try(:text).try(:strip)
            match_time_string += ' ' + game.css('span.suff span:last-child').try(:text).try(:strip)

            match_time = Time.zone.parse(date_string + " " + match_time_string)

            # GameCenter Link
            gc_div = game.css('div.schedules-list-content')
            gc_link = gc_div.attribute('data-gc-url').value
            match_key = gc_link.gsub(/https?:\/\/www.nfl.com\/gamecenter\//, '')

            if (match_key.present? && match_time.present? && home_team.present? && away_team.present?)
              match_obj = ::Golbot::Match.find_or_create_by(
                league: "nfl",
                key: match_key,
                home: home_team,
                away: away_team,
                date: match_time
              )
              matches_created << match_obj
              # if match_score.present?
              #   match_obj.update(score: match_score)
              # end
            end
          end
        end
      end
    end

    return matches_created
  end

  def self.get_nhl_matches_by_day(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    matches_created = []
    # games_url = "http://statsapi.web.nhl.com/api/v1/schedule"
    games_url = "https://statsapi.web.nhl.com/api/v1/schedule?startDate=#{year}-#{month}-#{day}&endDate=#{year}-#{month}-#{day}&expand=schedule.boxscore"

    uri = URI.parse(games_url)

    response = Net::HTTP.get_response(uri)

    if response.code == "200"
      dates = JSON.parse(response.body)['dates']
      unless dates.blank?
        dates.each do |date_object|
          games = date_object['games']
          games.each do |game|
            match_time = Time.zone.parse(game['gameDate'])
            match_key = game['gamePk']

            home_team = game['teams']['home']['team']['name']
            away_team = game['teams']['away']['team']['name']

            # statusCode 6 or 7 is Final (game completed)
            # 1 is Scheduled (not played yet)
            if(['6','7'].include?(game['status']['statusCode']))
              home_score = game['teams']['home']['score']
              away_score = game['teams']['away']['score']
              match_score = "#{home_score} - #{away_score}"
            end

            if (match_key.present? && home_team.present? && away_team.present?)
              match_obj = ::Golbot::Match.find_or_create_by(
                league: "nhl",
                key: match_key,
                home: home_team,
                away: away_team
              )
              matches_created << match_obj
              if match_time.present?
                match_obj.update(date: match_time)
              end
              if match_score.present?
                match_obj.update(score: match_score)
              end
            end
          end
        end
      end
    end

    return matches_created
  end

  def self.get_confed_cup_matches_by_day(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    matches_created = []

    games_url = "http://www.espnfc.us/fifa-confederations-cup/57/scores?date=#{year}#{month}#{day}"
    uri = URI.parse(games_url)

    response = Net::HTTP.get_response(uri)

    if response.code == "200"
      doc = Nokogiri::HTML.parse(response.body)

      matches = doc.css('.score-box')
      unless matches.blank?
        matches.each do |game|
          match_key = game.css('.score')[0].attribute('data-gameid').try(:value)

          home_team = game.css('.team-name')[0].try(:text).try(:strip)
          away_team = game.css('.team-name')[1].try(:text).try(:strip)

          match_time = game.css('.game-info .time').attribute('data-time').try(:value)

          unless match_time.blank?
            match_time = Time.zone.parse(match_time)
          else
            home_score = game.css('.team-score')[0].try(:text).try(:strip)
            away_score = game.css('.team-score')[1].try(:text).try(:strip)
            match_score = "#{home_score} - #{away_score}"
          end

          if (match_key.present? && home_team.present? && away_team.present? && match_time.present?)
            match_obj = ::Golbot::Match.find_or_create_by(
              league: "confed",
              key: match_key,
              home: home_team,
              away: away_team,
              date: match_time
            )
            matches_created << match_obj
            if match_score.present?
              match_obj.update(score: match_score)
            end
          end
        end
      end
    end

    return matches_created
  end

  def self.get_ucl_matches_by_day(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    matches_created = []

    # games_url = "http://scores.nbcsports.msnbc.com/epl/fixtures.asp?month=#{month}"
    games_url = "http://www.espnfc.us/uefa-champions-league/2/scores?date=#{year}#{month}#{day}"
    uri = URI.parse(games_url)

    response = Net::HTTP.get_response(uri)

    if response.code == "200"
      doc = Nokogiri::HTML.parse(response.body)

      matches = doc.css('.score-box')
      unless matches.blank?
        matches.each do |game|
          match_key = game.css('.score')[0].attribute('data-gameid').try(:value)

          home_team = game.css('.team-name')[0].try(:text).try(:strip)
          away_team = game.css('.team-name')[1].try(:text).try(:strip)

          match_time = game.css('.game-info .time').attribute('data-time').try(:value)
          unless match_time.blank?
            match_time = Time.zone.parse(match_time)
          end

          home_score = game.css('.team-score')[0].try(:text).try(:strip)
          away_score = game.css('.team-score')[1].try(:text).try(:strip)
          unless(home_score.blank? || away_score.blank?)
            match_score = "#{home_score} - #{away_score}"
          end

          if (match_key.present? && home_team.present? && away_team.present? && match_time.present?)
            match_obj = ::Golbot::Match.find_or_create_by(
              league: "ucl",
              key: match_key,
              home: home_team,
              away: away_team,
              date: match_time
            )
            matches_created << match_obj
          end
          if(match_score.present? && match_key.present?)
            match_obj = ::Golbot::Match.find_by(league: 'ucl', key: match_key)
            unless match_obj.nil?
              match_obj.update(score: match_score)
            end
          end
        end
      end
    end

    return matches_created
  end

  def self.get_epl_matches_by_day(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    matches_created = []

    # games_url = "http://scores.nbcsports.msnbc.com/epl/fixtures.asp?month=#{month}"
    games_url = "http://www.espnfc.us/english-premier-league/23/scores?date=#{year}#{month}#{day}"
    uri = URI.parse(games_url)

    response = Net::HTTP.get_response(uri)

    if response.code == "200"
      doc = Nokogiri::HTML.parse(response.body)

      matches = doc.css('.score-box')
      unless matches.blank?
        matches.each do |game|
          match_key = game.css('.score')[0].attribute('data-gameid').try(:value)

          home_team = game.css('.team-name')[0].try(:text).try(:strip)
          away_team = game.css('.team-name')[1].try(:text).try(:strip)

          match_time = game.css('.game-info .time').attribute('data-time').try(:value)
          unless match_time.blank?
            match_time = Time.zone.parse(match_time)
          end

          home_score = game.css('.team-score')[0].try(:text).try(:strip)
          away_score = game.css('.team-score')[1].try(:text).try(:strip)
          unless(home_score.blank? || away_score.blank?)
            match_score = "#{home_score} - #{away_score}"
          end

          if (match_key.present? && home_team.present? && away_team.present? && match_time.present?)
            match_obj = ::Golbot::Match.find_or_create_by(
              league: "epl",
              key: match_key,
              home: home_team,
              away: away_team,
              date: match_time
            )
            matches_created << match_obj
          end
          if(match_score.present? && match_key.present?)
            match_obj = ::Golbot::Match.find_by(league: 'epl', key: match_key)
            unless match_obj.nil?
              match_obj.update(score: match_score)
            end
          end
        end
      end
    end

    return matches_created
  end

  def self.get_mlb_matches_by_day(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    matches_created = []

    games_url = "http://mlb.mlb.com/gdcross/components/game/mlb/year_#{year}/month_#{month}/day_#{day}/master_scoreboard.json"
    uri = URI.parse(games_url)

    response = Net::HTTP.get_response(uri)

    if response.code == "200"
      games = JSON.parse(response.body)['data']['games']['game']

      unless games.blank?
        games.each do |game|
          if game[2].present?
            home_team = "#{game['home_team_city']} #{game['home_team_name']}"
            away_team = "#{game['away_team_city']} #{game['away_team_name']}"

            match_time = Time.zone.parse(game['time_date'] + ' ' + game['ampm'])
            match_key = game['gameday']

            match_status = game['status']['status']

            if(match_status.present? && match_status.downcase == "final")
              match_runs = game['linescore']['r']
              match_score = "#{match_runs['home']} - #{match_runs['away']}"
            end

            if (match_key.present? && home_team.present? && away_team.present?)
              match_obj = ::Golbot::Match.find_or_create_by(
                league: "mlb",
                key: match_key,
                home: home_team,
                away: away_team
              )
              matches_created << match_obj
              if match_time.present?
                match_obj.update(date: match_time)
              end
              if match_score.present?
                match_obj.update(score: match_score)
              end
            end
          end
        end
      end
    end

    return matches_created
  end

  def self.get_nba_matches_by_day(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    matches_created = []

    # http://www.cbssports.com/nba/gametracker/boxscore/NBA_20170423_CLE@IND/
    # http://www.cbssports.com/nba/scoreboard/20170423/
    # http://data.nba.net/data/10s/prod/v2/20170423/scoreboard.json
    teams_endpoint = "http://data.nba.net/data/10s/prod/v1/2017/teams.json"
    games_endpoint = "http://data.nba.net/data/10s/prod/v2/#{year}#{month}#{day}/scoreboard.json"

    teams_url = URI.parse(teams_endpoint)
    games_url = URI.parse(games_endpoint)

    teams_response = Net::HTTP.get_response(teams_url)
    games_response = Net::HTTP.get_response(games_url)

    if teams_response.code == "200" && games_response.code == "200"
      teams = JSON.parse(teams_response.body)['league']['standard']
      games = JSON.parse(games_response.body)['games']

      games.each do |game|
        home_team = teams.find{|t| t['teamId'] == game['hTeam']['teamId']}['fullName']
        away_team = teams.find{|t| t['teamId'] == game['vTeam']['teamId']}['fullName']

        # CBS Sports Key Format
        # NBA_20170423_CLE@IND
        match_key = "#{year}#{month}#{day}_#{game['vTeam']['triCode']}@#{game['hTeam']['triCode']}"
        # match_key = game['gameId']

        match_time = Time.zone.parse(game['startTimeUTC'])

        if game['statusNum'] == 1
          # Upcoming
        elsif game['statusNum'] == 3
          # Completed
          match_score = "#{game['hTeam']['score']} - #{game['vTeam']['score']}"
        end

        if (match_key.present? && home_team.present? && away_team.present?)
          match_obj = ::Golbot::Match.find_or_create_by(
            league: "nba",
            key: match_key,
            home: home_team,
            away: away_team
          )
          matches_created << match_obj
          if match_time.present?
            match_obj.update(date: match_time)
          end
          if match_score.present?
            match_obj.update(score: match_score)
          end
        end
      end
    end

    return matches_created
  end

  def self.get_mls_matches_by_day(day = Time.now.in_time_zone.strftime("%d"), month = Time.now.in_time_zone.strftime("%m"), year = Time.now.in_time_zone.to_date.year.to_i)
    matches_created = []

    # games_url = "http://scores.nbcsports.msnbc.com/epl/fixtures.asp?month=#{month}"
    games_url = "http://www.espnfc.us/major-league-soccer/19/scores?date=#{year}#{month}#{day}"
    uri = URI.parse(games_url)

    response = Net::HTTP.get_response(uri)

    if response.code == "200"
      doc = Nokogiri::HTML.parse(response.body)

      matches = doc.css('.score-box')
      unless matches.blank?
        matches.each do |game|
          match_key = game.css('.score')[0].attribute('data-gameid').try(:value)

          home_team = game.css('.team-name')[0].try(:text).try(:strip)
          away_team = game.css('.team-name')[1].try(:text).try(:strip)

          match_time = game.css('.game-info .time').attribute('data-time').try(:value)
          unless match_time.blank?
            match_time = Time.zone.parse(match_time)
          end

          home_score = game.css('.team-score')[0].try(:text).try(:strip)
          away_score = game.css('.team-score')[1].try(:text).try(:strip)
          unless(home_score.blank? || away_score.blank?)
            match_score = "#{home_score} - #{away_score}"
          end

          if (match_key.present? && home_team.present? && away_team.present? && match_time.present?)
            match_obj = ::Golbot::Match.find_or_create_by(
              league: "mls",
              key: match_key,
              home: home_team,
              away: away_team,
              date: match_time
            )
            matches_created << match_obj
          end
          if(match_score.present? && match_key.present?)
            match_obj = ::Golbot::Match.find_by(league: 'mls', key: match_key)
            unless match_obj.nil?
              match_obj.update(score: match_score)
            end
          end
        end
      end
    end

    return matches_created
  end

  def self.live_mlb_updates(channel, updates_json, last_update_id)
    if updates_json[:data]
      # Setup new Slack Client
      # Send to "channel"
      # Return latest id
    end
  end

  def self.twitter_update(league, team, since_tweet_id = nil)
    teams = {
      "ucl" => {
        "Atletico Madrid" => "atletienglish",
        "Leicester City" => "LCFC",
        "Real Madrid" => "realmadriden",
        "Bayern Munich" => "FCBayern",
        "Barcelona" => "FCBarcelona",
        "Juventus" => "juventusfcen",
        "AS Monaco" => "AS_Monaco_EN",
        "Borussia Dortmund" => "BVB",
        "Paris Saint-Germain" => "PSG_English",
        "Celtic" => "celticfc"
      },
      "mls" => {
        "Columbus Crew" => "ColumbusCrewSC",
        "Orlando City SC" => "OrlandoCitySC",
        "Chicago Fire" => "ChicagoFire",
        "New York City FC" => "NYCFC",
        "NY Red Bulls" => "NewYorkRedBulls",
        "Atlanta United FC" => "ATLUTD",
        "New England Revolution" => "NERevolution",
        "D.C. United" => "dcunited",
        "Toronto FC" => "torontofc",
        "Montreal Impact" => "impactmontreal",
        "Philadelphia Union" => "PhilaUnion",
        "Portland Timbers" => "TimbersFC",
        "Sporting Kansas City" => "SportingKC",
        "FC Dallas" => "FCDallas",
        "Houston Dynamo" => "HoustonDynamo",
        "San Jose Earthquakes" => "SJEarthquakes",
        "Real Salt Lake" => "RealSaltLake",
        "Vancouver Whitecaps FC" => "WhitecapsFC",
        "LA Galaxy" => "LAGalaxy",
        "Seattle Sounders" => "SoundersFC",
        "Minnesota United FC" => "MNUFC",
        "Colorado Rapids" => "ColoradoRapids"
      },
      "nba" => {
        "Washington Wizards" => "WashWizards",
        "Atlanta Hawks" => "ATLHawks",
        "Miami Heat" => "MiamiHEAT",
        "Charlotte Hornets" => "hornets",
        "Orlando Magic" => "OrlandoMagic",
        "Cleveland Cavaliers" => "cavs",
        "Indiana Pacers" => "Pacers",
        "Milwaukee Bucks" => "Bucks",
        "Chicago Bulls" => "chicagobulls",
        "Detroit Pistons" => "DetroitPistons",
        "Boston Celtics" => "celtics",
        "Toronto Raptors" => "Raptors",
        "New York Knicks" => "nyknicks",
        "Philadelphia 76ers" => "sixers",
        "Brooklyn Nets" => "BrooklynNets",
        "Golden State Warriors" => "warriors",
        "L.A. Clippers" => "LAClippers",
        "Sacramento Kings" => "SacramentoKings",
        "L.A. Lakers" => "Lakers",
        "Phoenix Suns" => "Suns",
        "San Antonio Spurs" => "spurs",
        "Houston Rockets" => "HoustonRockets",
        "Memphis Grizzlies" => "memgrizz",
        "New Orleans Pelicans" => "PelicansNBA",
        "Dallas Mavericks" => "dallasmavs",
        "Utah Jazz" => "utahjazz",
        "Oklahoma City Thunder" => "okcthunder",
        "Portland Trail Blazers" => "trailblazers",
        "Denver Nuggets" => "nuggets",
        "Minnesota Timberwolves" => "Timberwolves"
      },
      "epl" => {
        "Chelsea" => "ChelseaFC",
        "Tottenham Hotspur" => "SpursOfficial",
        "Liverpool" => "LFC",
        "Manchester City" => "ManCity",
        "Manchester United" => "ManUtd",
        "Arsenal" => "Arsenal",
        "Everton" => "Everton",
        "West Bromwich Albion" => "WBA",
        "Southampton" => "SouthamptonFC",
        "AFC Bournemouth" => "afcbournemouth",
        "Leicester City" => "LCFC",
        "Stoke City" => "stokecity",
        "Watford" => "WatfordFC",
        "Burnley" => "BurnleyOfficial",
        "West Ham United" => "WestHamUtd",
        "Crystal Palace" => "CPFC",
        "Hull City" => "HullCity",
        "Swansea City" => "SwansOfficial",
        "Middlesbrough" => "Boro",
        "Sunderland" => "SunderlandAFC"
      },
      "nhl" => {
        "New Jersey Devils" => "NJDevils",
        "New York Islanders" => "NYIslanders",
        "New York Rangers" => "NYRangers",
        "Philadelphia Flyers" => "NHLFlyers",
        "Pittsburgh Penguins" => "penguins",
        "Boston Bruins" => "NHLBruins",
        "Buffalo Sabres" => "BuffaloSabres",
        "Montréal Canadiens" => "CanadiensMTL",
        "Ottawa Senators" => "Senators",
        "Toronto Maple Leafs" => "MapleLeafs",
        "Carolina Hurricanes" => "NHLCanes",
        "Florida Panthers" => "FlaPanthers",
        "Tampa Bay Lightning" => "TBLightning",
        "Washington Capitals" => "Capitals",
        "Chicago Blackhawks" => "NHLBlackhawks",
        "Detroit Red Wings" => "DetroitRedWings",
        "Nashville Predators" => "PredsNHL",
        "St. Louis Blues" => "StLouisBlues",
        "Calgary Flames" => "NHLFlames",
        "Colorado Avalanche" => "Avalanche",
        "Edmonton Oilers" => "EdmontonOilers",
        "Vancouver Canucks" => "Canucks",
        "Anaheim Ducks" => "AnaheimDucks",
        "Dallas Stars" => "DallasStars",
        "Los Angeles Kings" => "LAKings",
        "San Jose Sharks" => "SanJoseSharks",
        "Columbus Blue Jackets" => "BlueJacketsNHL",
        "Minnesota Wild" => "mnwild",
        "Winnipeg Jets" => "NHLJets",
        "Arizona Coyotes" => "ArizonaCoyotes",
        "Vegas Golden Knights" => "GoldenKnights"
      }
    }

    if teams.key?(league) && teams[league].key?(team)
      require 'twitter'

      handle = teams[league][team]

      twitter_client = Twitter::REST::Client.new do |config|
        config.consumer_key        = "knU72NnfCvi3eB2Cad5hhhceM"
        config.consumer_secret     = "wHDChipY0VvIMVqwvLcJazSYlFbHE0YL7yGDkh9atmXhyzyh84"
        config.access_token        = "4825556590-uOUX8vywKbkl4NI9FMTI0k5MEi7zs0EZme1HjD2"
        config.access_token_secret = "xv9TZGkTMajKPQuTB5vIrzLVqwvbzmfx2F29zyqmed9c0"
      end

      if since_tweet_id.nil?
        latest_tweet = twitter_client.search("from:#{handle} filter:media", result_type: 'recent').first
      else
        latest_tweet = twitter_client.search("from:#{handle} filter:media", result_type: 'recent', since_id: since_tweet_id).first
      end

      return latest_tweet
    else
      return false
    end
  end

  def self.find_matches_by_date(date = Time.now.in_time_zone.to_date)
    matches = ::Golbot::Match.where("date > ? AND date < ?", date.at_beginning_of_day, date.at_end_of_day).order(date: :asc)
  end

  def self.find_matches_by_league_and_date(league, date = Time.now.in_time_zone.to_date)
    matches = ::Golbot::Match.where(league: league).where("date > ? AND date < ?", date.at_beginning_of_day, date.at_end_of_day).order(date: :asc)
  end

  def self.find_matches_by_league_and_teams(league, team1, team2)
    replacements = [
      ['utd', 'united']
    ]

    replacements.each do |c|
      team1.gsub!(c[0], c[1])
      team2.gsub!(c[0], c[1])
    end

    team1.gsub!('the', '')
    team2.gsub!('the', '')

    team1.gsub!(/\s+/, '%')
    team2.gsub!(/\s+/, '%')

    matches = ::Golbot::Match.where(league: league)
      .where("((home ILIKE ?) AND (away ILIKE ?)) OR ((home ILIKE ?) AND (away ILIKE ?))", '%'+team1+'%', '%'+team2+'%', '%'+team2+'%', '%'+team1+'%')
      .order(date: :asc)
  end

  def self.find_matches_by_team(team)
    replacements = [
      ['utd', 'united']
    ]

    replacements.each do |c|
      team.gsub!(c[0], c[1])
    end

    team.gsub!('the', '')
    team.gsub!(/\s+/, '%')

    matches = ::Golbot::Match.where("home ILIKE ? OR away ILIKE ?", '%'+team+'%', '%'+team+'%').order(date: :asc)
  end

  def self.find_matches_by_league_and_team(league, team)
    replacements = [
      ['utd', 'united']
    ]

    replacements.each do |c|
      team.gsub!(c[0], c[1])
    end

    team.gsub!('the', '')
    team.gsub!(/\s+/, '%')

    matches = ::Golbot::Match.where(league: league).where("home ILIKE ? OR away ILIKE ?", '%'+team+'%', '%'+team+'%').order(date: :asc)
  end

  def self.find_matches_by_teams(team1, team2)
    replacements = [
      ['utd', 'united']
    ]

    replacements.each do |c|
      team1.gsub!(c[0], c[1])
      team2.gsub!(c[0], c[1])
    end

    team1.gsub!('the', '')
    team2.gsub!('the', '')

    team1.gsub!(/\s+/, '%')
    team2.gsub!(/\s+/, '%')

    matches = ::Golbot::Match.where("((home ILIKE ?) AND (away ILIKE ?)) OR ((home ILIKE ?) AND (away ILIKE ?))", '%'+team1+'%', '%'+team2+'%', '%'+team2+'%', '%'+team1+'%').order(date: :asc)
  end

  def self.find_completed_matches_by_teams(team1, team2)
    replacements = [
      ['utd', 'united']
    ]

    replacements.each do |c|
      team1.gsub!(c[0], c[1])
      team2.gsub!(c[0], c[1])
    end

    team1.gsub!('the', '')
    team2.gsub!('the', '')

    team1.gsub!(/\s+/, '%')
    team2.gsub!(/\s+/, '%')

    matches = ::Golbot::Match.where('score IS NOT NULL')
      .where("((home ILIKE ?) AND (away ILIKE ?)) OR ((home ILIKE ?) AND (away ILIKE ?))", '%'+team1+'%', '%'+team2+'%', '%'+team2+'%', '%'+team1+'%')
      .order(date: :asc)
  end

  def self.find_upcoming_matches_by_teams(team1, team2)
    replacements = [
      ['utd', 'united']
    ]

    replacements.each do |c|
      team1.gsub!(c[0], c[1])
      team2.gsub!(c[0], c[1])
    end

    team1.gsub!('the', '')
    team2.gsub!('the', '')

    team1.gsub!(/\s+/, '%')
    team2.gsub!(/\s+/, '%')

    matches = ::Golbot::Match.where("((home ILIKE ?) AND (away ILIKE ?)) OR ((home ILIKE ?) AND (away ILIKE ?)) AND score IS NULL", '%'+team1+'%', '%'+team2+'%', '%'+team2+'%', '%'+team1+'%').order(date: :asc)
  end
end
