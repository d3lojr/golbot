#! /bin/bash

COUNT="$(ps -U golbot -u golbot u | grep AppPreloader | wc -l)"

if [ $COUNT -gt 1 ]; then
	echo bot running. pinging
	curl -I localhost:81/ping 2>/dev/null | head -n 1 | cut -d$' ' -f2
else
	echo bot restarting
	passenger-config restart-app /home/golbot/golbot
fi
