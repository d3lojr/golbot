class Golbot::Tracker < ActiveRecord::Base
  def self.table_name_prefix
    'golbot_'
  end

  def ping(client)
    client.say(channel: channel, text: "Pinged match #{key}")
  end
end
