class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  respond_to :json

  def dashboard
    matches = []
    date = Time.now.in_time_zone.to_date

    games = ::Golbot::Match
      .where("date > ? AND date < ?", date.at_beginning_of_day, date.at_end_of_day + 3.days)
      .where("(((home ILIKE ?) OR (away ILIKE ?)) OR ((home ILIKE ?) OR (away ILIKE ?)))", '%atlanta%', '%atlanta%', '%falcons%', '%falcons%')
      .order(date: :asc)

    unless games.blank?
      games.each do |game|
        if game.date.present?
          matches << ["#{game.league.upcase}", "#{game.home} vs #{game.away}", "#{game.date.in_time_zone.strftime('%a, %b %-d, %-l:%M %P')}"]
        else
          matches << ["#{game.league.upcase}", "#{game.home} vs #{game.away}", "Time Unavailable"]
        end
      end
    end

    respond_with matches.as_json
  end

  def monthly
    date_now = Time.now.in_time_zone.to_date
    date_start = Date.new(date_now.year, date_now.month, date_now.day)
    date_end = Date.new((date_now + 30.days).year, (date_now + 30.days).month, (date_now + 30.days).day)

    matches = []

    (date_start..date_end).each do |date|
      get_matches = Bot.get_nhl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_mls_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_nba_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_mlb_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_epl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_ucl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    # (date_start..date_end).each do |date|
    #   get_matches = Bot.get_confed_cup_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    #   matches += get_matches unless get_matches.nil?
    # end

    render json: matches.to_json
  end

  def daily
    matches = []

    get_matches = Bot.get_nfl_matches
    matches += get_matches unless get_matches.nil?

    # Loop over every day from the 1st to the last
    date_now = Time.now.in_time_zone.to_date
    date_start = Date.new((date_now - 7.days).year, (date_now - 7.days).month, (date_now - 7.days).day)
    date_end = Date.new((date_now + 7.days).year, (date_now + 7.days).month, (date_now + 7.days).day)

    (date_start..date_end).each do |date|
      get_matches = Bot.get_nhl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_mls_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_nba_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_mlb_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_epl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    (date_start..date_end).each do |date|
      get_matches = Bot.get_ucl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
      matches += get_matches unless get_matches.nil?
    end

    # (date_start..date_end).each do |date|
    #   get_matches = Bot.get_confed_cup_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    #   matches += get_matches unless get_matches.nil?
    # end

    render json: matches.to_json
  end

  def hourly
    matches = []

    date = Time.now.in_time_zone
    get_matches = Bot.get_nba_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    matches += get_matches unless get_matches.nil?
    get_matches = Bot.get_mlb_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    matches += get_matches unless get_matches.nil?
    get_matches = Bot.get_nhl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    matches += get_matches unless get_matches.nil?
    get_matches = Bot.get_epl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    matches += get_matches unless get_matches.nil?
    get_matches = Bot.get_mls_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    matches += get_matches unless get_matches.nil?
    matches += Bot.get_ucl_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    # get_matches = Bot.get_confed_cup_matches_by_day(date.strftime('%d'), date.strftime('%m'), date.year.to_i)
    # matches += get_matches unless get_matches.nil?

    render json: matches.to_json
  end

  def ping
    Thread.current[:ping_count] ||= 0
    Thread.current[:ping_count] += 1

    # Twitter updates
    trackers = ::Golbot::Tracker.all

    unless trackers.empty?
      trackers.each do |tracker|
        [tracker.home, tracker.away].each do |team|
          latest_tweet = Bot.twitter_update(tracker.league, team, tracker.latest_tweet)
          unless latest_tweet.blank?
            unless latest_tweet.id == tracker.latest_tweet
              client = Slack::Web::Client.new(token: Team.first.token)
              client.chat_postMessage(channel: tracker.channel, text: latest_tweet.uri, as_user: true)
              tracker.update(latest_tweet: latest_tweet.id.to_s)
            end
          end
        end
      end
    end

    trackers = ::Golbot::Tracker.where("start <= ?", Time.now)

    unless trackers.empty?
      client = Slack::Web::Client.new(token: Team.first.token)

      trackers.each do |tracker|
        last_event_id = tracker.last_event
        comments = []
        match_ended = false

        case tracker.league

        when 'nhl'
          # http://statsapi.web.nhl.com/api/v1/game/#{tracker.key}/feed/live
        when 'mlb'
          # http://www.mlb.com/gdcross/components/game/mlb/year_2017/month_04/day_01/gid_2017_04_01_detmlb_miamlb_1/game_events.json
        when 'epl', 'ucl', 'mls'
          # http://scores.nbcsports.msnbc.com/epl/boxscore.asp?gamecode=2017040110025&show=commentary
          uri = URI.parse("http://www.espnfc.us/commentary?gameId=#{tracker.key}")

          result = Net::HTTP.get_response(uri)

          if result.code == "200"
            doc = Nokogiri::HTML.parse(result.body)

            commentary = doc.css('article.match-commentary .active table tr')
            unless commentary.blank?
              commentary.each do |comment|
                comment_id = comment.attribute('data-id').try(:value).split('-').try(:last)
                comment_type = comment.attribute('data-type').try(:value)
                time = comment.css('.time-stamp').try(:text).try(:strip)
                # text = "*[#{comment_type.titleize}]*: " + comment.css('.game-details').try(:text).try(:strip)
                text = comment.css('.game-details').try(:text).try(:strip)

                if comment_type == "yellow-card"
                  text = ":yellowcard: " + text
                elsif comment_type == "goal"
                  text = ":soccer: " + text
                elsif comment_type == "substitution"
                  text = ":arrows_clockwise: " + text
                end

                unless text.blank?
                  comments << {id: comment_id, time: time, text: text}
                end
              end
            end
          end
        when 'nba'
          uri = URI.parse("http://www.cbssports.com/nba/gametracker/live/NBA_#{tracker.key}/")
          # This url will return 302 when match is not in progress

          result = Net::HTTP.get_response(uri)

          if result.code == "200"
            doc = Nokogiri::HTML.parse(result.body)
            score_update = false

            score_home = doc.css('.team-score-container.home .score-text').try(:text).try(:strip)
            score_away = doc.css('.team-score-container.away .score-text').try(:text).try(:strip)

            team_home = doc.css('.team-name-container.home .nickname').try(:text).try(:strip)
            team_away = doc.css('.team-name-container.away .nickname').try(:text).try(:strip)

            comments_object = doc.css('table.play-item')
            unless comments_object.blank?
              comments_object.each do |comment|
                text = comment.css('td.description').try(:text).try(:strip)
                time = comment.css('td.time-remaining').try(:text).try(:strip)
                points = comment.css('td.points').try(:text).try(:strip)
                unless points.blank?
                  text = text + " (#{points})"
                  score_update = true
                end
                id = text.to_i(36)
                unless text.blank?
                  comments << {id: id, time: time, text: text}
                end
              end

              unless((score_home.blank? || score_away.blank?) || !score_update)
                if score_home.to_i > score_away.to_i
                  comments.unshift({id: comments.last[:id], time: 'Score Update', text: "#{team_home}_ (*#{score_home}*) - (#{score_away}) _#{team_away}" })
                else
                  comments.unshift({id: comments.last[:id], time: 'Score Update', text: "#{team_home}_ (#{score_home}) - (*#{score_away}*) _#{team_away}" })
                end
              end
            end
          end
        end

        unless comments.blank?
          caught_up = false
          comments_tmp = comments.reverse
          comments = []
          comments_tmp.each do |comment|
            if (comment[:text].present? && (comment[:text].include?("Match end") || comment[:text].include?("Second Half ends")))
              match_ended = true
            end

            if (tracker.last_event == comment[:id] || tracker.last_event == nil)
              caught_up = true
              next unless tracker.last_event == nil
            end

            if caught_up == true
              unless comment[:time].blank?
                comments << "[*#{comment[:time]}*] _#{comment[:text]}_"
              else
                comments << "[*#{tracker.home}* vs *#{tracker.away}*] _#{comment[:text]}_"
              end
              last_event_id = comment[:id]
            end
          end

          unless comments.blank?
            client.chat_postMessage(channel: tracker.channel, text: comments.join("\r\n"), as_user: true)
          end
        end

        if last_event_id != tracker.last_event
          tracker.update(last_event: comments_tmp.last[:id])
        end

        if match_ended
          client.chat_postMessage(channel: tracker.channel, text: "[*#{tracker.home}* vs *#{tracker.away}*] Match has ended. Killing tracker.", as_user: true)
          tracker.destroy
        end
      end
    end

    render text: Thread.current[:ping_count]
  end
end
