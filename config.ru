# This file is used by Rack-based servers to start the application.
require ::File.expand_path('../config/environment', __FILE__)
require ::File.expand_path('../golbot', __FILE__)

ENV['SLACK_API_TOKEN'] = Rails.application.secrets.slack_api_token
ENV['DATABASE_ADAPTER'] = 'activerecord'

SlackRubyBotServer::App.instance.prepare!
SlackRubyBotServer::Service.start!

run SlackRubyBotServer::Api::Middleware.instance

run Rails.application